import { atom } from 'recoil'

export const dutiesList = atom({
  key: '@list/duties',
  default: [],
})
